"""

Create methods that separate crime data

1. Create a method that separates each line of data (create a dict with Record ID as key, list as value) and cleans

2. Create a method to separate data by zip code and return max crime types by zip code


"""

import re


class Crime:

    def __init__(self, record_id, report_date, report_time, major_offense_type, address, neighborhood, police_precinct,
                 police_district, x_coord, y_coord, *args, **kwargs):
        self.record_id = record_id
        self.report_date = report_date
        self.report_time = report_time
        self.major_offense_type = major_offense_type
        self.address = address
        self.neighborhood = neighborhood
        self.police_precinct = police_precinct
        self.police_district = police_district
        self.x_coord = x_coord
        self.y_coord = y_coord
        self.zip_code = self.zip_finder() """ TODO: figure out error:  Traceback (most recent call last):
                                              File "<stdin>", line 1, in <module>
                                              File "/Users/Dizz/Desktop/CodeGuild/labs/crime/crime.py", line 72, in crime_from_strings
                                                record = Crime(*record)
                                              File "/Users/Dizz/Desktop/CodeGuild/labs/crime/crime.py", line 29, in __init__
                                                self.zip_code = self.zip_finder()
                                              File "/Users/Dizz/Desktop/CodeGuild/labs/crime/crime.py", line 41, in zip_finder
                                                self.zip_code = match.group(2)
                                            AttributeError: 'NoneType' object has no attribute 'group' """


    def __str__(self):
        return "Crime record {0.record_id}, Major Offense Type: {0.major_offense_type}" \
               ", ZIP Code: {0.zip_code}".format(self)

    def __repr__(self):
        return "{0.__class__.__name___({0.record_id}, {0.report_date}, {0.report_time}, {0.major_offense_type}, {0.address}, " \
               "{0.neighborhood}, {0.police_precinct}, {0.police_district}, {0.x_coord}, {0.y_coord})".format(self)

    def zip_finder(self):
        match = re.match('(\d+-.*\s\w.\s)(\d{5})', self.address)
        self.zip_code = match.group(2)
        return self.zip_code


class CrimeData:

    def __init__(self, filename, *args, **kwargs):
        self.filename = filename
        self.data = None
        self.crimes = None

    def __str__(self):
        return "List of crime data from {0.filename}".format(self)

    def clean_data(self):
        """
        opens and cleans .csv file, adds each line to a list, and removes quotations.

        """
        with open(self.filename, 'r', encoding='utf-8') as f:
            f = f.readlines()
            data = [x.replace("\n", "") for x in f]
            data.remove(data[0])

            self.data = data
        return self.data

    def crime_from_strings(self):
        new_crime_list = []
        for record in self.data:
            record = record.split(",")
            record = Crime(*record)
            new_crime_list.append(record)
            self.crimes = new_crime_list

        return self.crimes



